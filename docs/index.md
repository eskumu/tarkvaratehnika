# Tarkvaratehnika kordamisküsimused

> Parandusi ja täiustusi saab teha [GitLabi reposse](https://gitlabcom/eskumu/tarkvaratehnika) 

## 1. Business Value Delivery in Waterfall and Iterative Methods

Kosemudelil arendades on tarkvara äriline väärtus 0 senikaua kuni toode ei ole valmis. 
Iteratiivselt arendades on tarkvaral väärtust ka siis, kui kogu funktsionaalsus ei ole veel valmis.

Kosemudelil tehakse esmalt valmis kogu tarkvara analüüs, seejärel disain, seejärel kood ning lõpuks testid.
Iteratiivselt tehakse neid tarkvara arenduse osasi paralleelselt/väiksemate osadena, ning iga osa/iteratsiooni lõpuks kasvab tarkvara äriline väärtus.

![1557658228777](assets/1557658228777.png)

## 2. Risks in Waterfall and Iterative Methods

Iteratiivsel arendusel tehakse riskantsed asjad esmalt ning projekti õnnestumise risk väheneb kiiremini.

Kosemudelil arendades on oht, et 

1. Tehakse valet asja, kuna kliendi tagasisidet saab vaid siis, kui toode on lõpetatud.
2. Tehakse rohkem kui vaja, kuna kosemudeli puhul on hiljem projekti muutmine kallis, tehakse kohe projekt suurelt, lisades ebavajalikke võimalusi.

## 3. The Cost of Change in Waterfall and Iterative Methods

Kosemudelil on muutus odav, kui ollakse veel analüüsi faasis, hiljem on muutus kallis, kuna muudatusi tuleb teha kõikides arendusetappides. Hilisemad muudatused võivad olla iteratiivse arenguga võrreldes kuni 100 korda kallimad.

![1557658170114](assets/1557658170114.png)

## 4. Extreme Programming Values

1. Kommunikatsioon (*communication*). 
   Oluline kõikide tarkvaraprotsessis olevate osapoolte vahel. Suhtlus kliendi-arendaja-managementi vahel. Vaid suheldes saavad kõik aru toote eesmärgist (*intent*).
2. Lihtsus (*simplicity*) . 
   Realiseerige kõige lihtsam varsioon kliendi vajaduste põhjal. Keerukus tekitab lisaprobleeme.
3. Tagasiside (*feedback*). 
   Kõige võimsam kvaliteedi instrument on tagasiside. Hea tagasisidega avastatakse probleemid kõige varaseimal võimalikul hetkel.
   Tagasiside liigid:
   - Release plan (kuud)
   - iteratsiooni plaan (nädalad)
   - Acceptance tests (päevad)
   - Stand-up (iga päev)
   - Paaris arutamine (tunnid)
   - Unit-tests (minutid)
   - Paarisprogemine (kohene tagasiside)
4. Julgus (*courage*). 
   Julgus proovida uusi ideid ja lähenemisi. (**Ekstreemne** tarkvaralooja). Kommunikatsioon või tagasiside ütlevad, kui midagi ei tööta.

## 5. Extreme Programming Practices

1. Planning game
2. Small releases
3. Metaphor (Vision)
4. Simple design
5. Refactoring
6. Test-First Development (TDD)
7. Pair programming
8. Collective ownership
9. Continuous integration
10. 40-hour workweek
11. On-site customer
12. Coding standards

## 6. Extreme Programming: Metaphor

Visioon. Nagu suur US. Millest saavad ühtemoodi aru kliendid ja arendajad.
*Elevator statement*. Kirjeldab toodet kahe minutiga.

## 7. Extreme Programming: Test-Driven Development

Unit testid kirjutatakse enne koodi. On üks tagasiside viise. US jaoks kirjutatud testid käituvad vastuvõtutestidena, aitavad täpsustada funktsionaalsust.

TDD muster:

![1557660301062](assets/1557660301062.png)

## 8. Extreme Programming: Pair Programming

Kaks programmeerijat töötavad ühe arvuti taga.

Üks on driver (hiir ja klaviatuur / mõtleb järgmise LoC peale ja API peale.) ja  teine navigator(jälgib tööd/ mõtleb alternatiivide peale/ testide peale/ nõuete peale).

Pidev tagasiside, nii loodud kood on kvaliteetsem ja saab kiiremini valmis.

Meeskonnas peavad paarid pidevalt vahetuma.

## 9. Extreme Programming: Refactoring

Tehniline praktika koodi lihtsamaks tegemiseks. Kirjutame koodi ümber jättes sama funktsionaalsuse. Või ka koodi omaduste täiendamine. Eelduseks on kogu funktsionaalsust katvad ühiktestid.
Bad smells in code:

- Long methods / classes
- duplicate code
- methods does several different things
- too much dependencies
- complex / unreadable code

## 10. Extreme Programming: Simple Design

Esmalt tehakse valmis kõige lihtsaim töötav variant. Nii saab küsida kliendi käest kiiret tagasisidet, hiljem võidakse refaktoreerimisega koodi muuta.

## 11. Extreme Programming: Collective Code Ownership

Kood kuulub projektile/meeskonnale, mitte ühele programmeerijale. Kõik programmeerijad võivad kogu koodi muuta. Kõik vastutavad kogu koodi eest.

## 12. Extreme Programming: Continuous Integration

Koodijupp integreeritakse **arenduskeskkonda** niipea, kui Unit Testid passivad. (Commit early and often).
Edasiarendus CD (tootmiskeskonda *live*). 

## 13. Extreme Programming: On-Site Customer

Kliendid on kohapeal (US arutlemisel, teeb kriitilisi äriotsuseid), arendajad ei pea otsuseid ootama, face-to-face vestlus vähendab arusaamatusi.

## 14. Extreme Programming: Small Releases

Release toimub tihedalt. Ajaliselt piiratud. Võimalikult väike, aga ärilise väärtusega. Kiire kliendi tagasiside, kas tegime õiget asja?

## 15. Extreme Programming: 40-Hour Work Week

Tööpäev peab olema väsitav (intensiivne), kuid ei tohi võtta rohkem aega. Arendajale peab jääma vaba aega.

## 16. Scrum Framework

+ Roles (vt p 17)
+ Sprint Ceremonies (vt p 18)
+ Artifacts (vt p 19)

## 17. Scrum Framework: Roles

Roles

- Product owner – defines features, decides release date, ROI, accept/reject work
- ScrumMaster – project manager, shields the team from external interferences
- The team – 5-9 people, cross functional, fulltime, self-organizing, stable for sprint

Stackeholders:

- Users, Management, Marketing.

## 18. Scrum Framework: Ceremonies

Sprint Ceremonies

- Planning – selecting items from product backlog (into sprint backlog), collaboratively
- Review – demo, whole team, informal, no slides
- Retrospective – whole team, what is and what isnt working, 15-30min (start/stop/continue)
- Daily meeting – 15min, standup, everybody is invited but only members can speak (3 questions, what did you do, what will you do, is anything in way)

## 19. Scrum Framework: Artifacts

Artifacts

- Product backlog – requirements, re-prioritized at every sprint start
- Sprint backlog – what shall be done this sprint, created by team members, daily
  update. Work is never assigned
- Sprint goal – short statement of focus for this sprint
- Burndown charts – shows total sprint backlog hours remaining, ideally should go down to 0 at sprint end (can go UP)

## 20. Scrum: Sprint Planning

Product owner ja tiim valivad koos product backlogist *user story*si, mida nad järgmises sprindis lõpetada lubavad. Product owner saab lõppsõna.

Iga US ajakulu hinnatakse. US jagatakse väiksemateks osadeks (tasks), ning need jagatakse meeskonna vahel ära. 

## 21. Scrum: Potentially Shippable Product Increment

Sprindi lõpuks valmis olev toote parem versioon, mis peab olema piisavalt kvaliteetne, et selle võib panna live keskkonda. *Potentially* selle pärast, et live paneku üle otsustab äripool.

## 22. Scrum:The Daily Scrum

15 min, standup, everybody is invited but only members can speak.

3 questions

- what did you do?
- what will you do?
- is anything in way

## 23. Sprint Review

Meeskond näitab sprindi jooksul tehtud tööd (demo).

Mitteformaalne, valmistatakse ette 2h max, ilma slaiditeta.

Kogu meeskond on kohal, kõik teised on ka kutsutud/oodatud.

PO võtab sprindi vastu või lükkab tagasi.

## 24. Scrum: Sprint Retrospective

Meeskonna enesereflektsioon iga sprindi lõpus (15-30 min). Kogu meeskond on kohal.

Kuidas järgmine sprint paremaks teha? Millega jätkata, millega alustada, mida lõpetada?

## 25. MoSCoW rules

Story prioritization technique referred to as the MoSCoW rules. 

- Must have - critical to the current delivery timebox in order for it to be a
  success.
- Should have - mportant but not necessary for delivery in the current delivery
  timebox. 
- Could have - desirable but not necessary, and could improve user experience or
  customer satisfaction for little development cost.
- Won't have this time

2/3 of implemented features should be in must have category.

![1557665728355](assets/1557665728355.png)

## 26. Advantages of Short Iterations

- More effective **feedback**. (feedback loop is shorter)
- More effective iterations (sprints) **planning**.

## 27. Disadvantages of Short Iterations

- **Smaller stories** are often too small to be valuable and difficult to identify
- In the **backlog of smaller stories** could be hundreds of items, instead of couple dozen in Kent Beck’s days (extreme programming).  Problem of management of stories has emerged.

## 28. Kanban: Primary Goal

Kanban primary goal is to eliminate waste.

Any activity that consumes resources, but creates no value is waste.

## 29. Kanban: 7 Wastes

![1557666199427](assets/1557666199427.png)



## 30. Kanban: Pull value through the Value Stream

Pull produces only as much as is needed in the next step.

- Don’t build features that nobody needs right now
- Don’t write more specs than you can code
- Don’t write more code than you can test
- Don’t test more code than you can deploy

## 31. Kanban: Limit WIP

Work In Progress. 

Limit how many work items are at any stage – reduce multi-tasking, maximize throughput, enhance teamwork.

## 32. Kanban: Minimal Marketable Feature

A minimal marketable feature (MMF) is a chunk of functionality that delivers a subset of the customer’s requirements, and that is capable of returning value to the customer when released as an independent entity.

**“Marketable”** means that it provides significant value to the customer; value may include revenue generation, cost savings, competitive differentiation, brand-name projection, or enhanced customer loyalty. 

## 33. Kanban: Cycle Time and Lead Time

The Lead time is the time from the moment when the request was made by a client and placed on a board to when all work on this item is completed and the request was delivered to the client. It's the **total time the client is waiting for an item to be delivered**.

The Cycle time is the amount of time, that the **team spent actually working on this item** (without the time that the task spent waiting on the board). Therefore, the Cycle time should start being measured, when the item task enters the "working" column, not earlier.

![1558353922738](assets/1558353922738.png)

## 34. Kanban: Kaikaku and Kaizen

In lean terms, there are two kinds of improvement.

Kaizen is evolutionary, focused on **incremental improvements**.

Kaikaku is revolutionary, focused on **radical improvements**.

## 35. Lean STARTUP: Customer Development Process

- Customer discovery (leia probleem, lahendus)

- Customer validation (Kas probleem on ikka olemas? Kas saab raha teenida? kui ei siis pivot ja uuesti probleemi lahendama)
- Customer creation (leia maksvaid kliente)
- Company building (skaleeri firmat)

![1558367400893](assets/1558367400893.png)

## 36. Lean STARTUP: Lifecycle

Build -> Measure -> Learn .. -> Build

![1558367726913](assets/1558367726913.png)

## 37. Lean STARTUP: Minimum Viable Product

- Version of product that enables a **full turn** of the Build – Measure – Learn loop with a **minimum** amount of **effort** and the least amount of development time.
- Has **core features** which allow the product to be deployed.
- Helps in avoiding building a product that the customers **do not want**.
- Can be deployed to **Early Adopters** that are thought to be more likely to provide **feedback**.

![1558367981215](assets/1558367981215.png)

## 38. Lean STARTUP: Principles

- **Eliminate Waste**. Don’t waste a bunch of time building something that no one might want.
- **Create Knowledge**. Lean startups are all about learning.
- **Short Iterations** with a tight feedback loop to allow us to learn and adapt.
- **Fail Fast!** If something isn’t going to work, figure it out as soon as possible so we can move on to something that will.

## 39. The Structure of Lean UX

1. Surface - visual design(usually the part people think of when you say ‘’ Web design‘’.
2.  Skeleton – Information design, Interface design, navigation design.
3. Structure – Interaction Design, Information Architecture  (how the user moves form one step in a process and content element to the next
4. Scope – Functional Specifications(application features), Content requirements(content  elements the site must include)
5. Strategy – User Needs (what the site must do for users) Site Objectives (what the site must    do for the people who build it)

![1558368173133](assets/1558368173133.png)



## 40. 5 Principles of Lean Thinking

Define value → Map Value Stream → Create Flow →  Establish Pull → Pursue Perfection

// TODO kust see info pärit on?

## 41. Poka-Yoke–Early Error Detection

1. Errors will not turn into defects if feedback and action take place at the error stage.
2. Do it right the first time (finding problems early is cheaper, test automation, hard to add quality later)

## 42. Lean Thinking Tools

1. Lean Thinking.
   Thinking about making **value** flows through the system, **improving processes continuously** and **eliminating waste** progressively.
   Reduce non value added waste.

2. Theory of constraints.
   Leveraging systems by being aware of their bottlenecks.

3. Queueing Theory
   The average time in the system is equal to the average time in queue plus the average time it takes to receive service.
   To reduce the cycle time

   1. Reduce number of things in processes.
   2. Improve average completion time.

   ![1558369555964](assets/1558369555964.png)

## 43. Principles of Lean Software Development

1. Eliminate waste
2. Amplify learning
3. Decide as late as possible
4. Deliver as fast as possible
5. empower the team
6. Build quality in 
7. See the whole

## 44. DevOps Principles

#### **The Three Ways**

- Systems Thinking.
  Kõik meeskonnad ja inimesed organisatsioonis peavad nägema oma tööd kui osana tervikust. Näiteks arendaja peab lisaks koodi kirja panemisele ka mõistma, miks sellist funktsionaalsust vaja on ning kuidas see mõjutab kogu süsteemi.
- Amplify Feedback Loops.
  Tagasiside on oluline, tagasiside jaoks on erinevaid vorme, mis on kõik tähtsad. Näiteks code-review ja automaattestid/test keskkond. Samas on oluline ka see, et kõik tunneksid klienti ja nende probleeme, selleks osalevad klienditoe töös ka näiteks arendajad, nii õpivad arendajad paremini klienti tundma.
- Culture of Continual Experimentation
  Keskkond, kus ideede välja toomine ja katsetamine on lubatud ja julgustatud. Eksimist nähakse võimalusena õppida.

#### CAMS 

- **Culture** – People > Process > Tools.
  Suhtlus on oluline, devOpsi vajadust peavad kõik mõistma. Meeskonnad peavad üksteise vajadusi mõistma.

- **Automation** – Infrastructure as Code

  DevOpsi eesmärk on võimalikult kiirelt ja turvaliselt saada kood productionisse.
  Deployment peab olema korratav, kuna siis on erinevad keskkonnad võimalikult samasugused (prod, test, CI, dev arvuti), manuaalselt võivad tekkida vead/erinevused.
  Deployment peab olema kiire, järelikult automaatne süsteem võimaldab seda kõige paremini.
  Deployment peab olema turvaline, kui kõik keskkonnad on võimalikult productioni moodi, siis on suurem tõenäosus, et vead avastatakse varem. 

- **Measurement** – Measure Everything
  Mõõtes saab avastada muutusi süsteemis, ning leida vigu. Mõõta ei tuleks vaid süsteemi tehnilisi omadusi, nt kiirust, mälukasutust vms, vaid ka süsteemi arilisi mõõdikuid, näiteks kasumit.

- **Sharing** – Collaboration/Feedback
  DevOpsi jaoks on ka vajalik, et tarkvaras leitud vigade puhul ei süüdistata vea tegijad, vaid leitakse viise, kuidas süsteem saab tulevikus selliseid vigu avastada enne, kui need live keskkonda jõuavad.

## 45. Continuous Integration, Delivery and Deployment

![1558467471038](assets/1558467471038.png)

#### Continous integration. 

- **Continuous Integration** is a software development practice where members of a team integrate their work **frequently**, usually each person integrates at least daily - leading to multiple integrations per day.
- Each integration is **verified** by an **automated build** (including test) to detect integration errors as quickly as possible. 

#### Continous delivery

Kood jõuab testkeskkonda automaatselt.

#### Continous deployment

Kood jõuab ka *live*-keskkonda automaatselt. 

## 46. Continuous Testing

- Test environment provisioning and configuration
- test data management.
- Integration, functional, performance and security testing.

![1558467804820](assets/1558467804820.png)

Automatiseeritud on ka testimise osa, kus teste jooksutatakse automaatselt, enne kui uus kood pannakse mingisse keskkonda. Arendaja veendub esmalt, et ühiktestid läbivad. Testkeskkonnas kontrollitakse automaatselt ühikteste, integratsiooni, acceptance jne teste.  

## 47. What is Architecture?

Anarchitecture is the set of **significant decisions** about the organization of a software system, the **selection of the structural elements and their interfaces** by which the system is composed, together **with their behavior** as specified in the collaborations among those elements, the **composition of these structural elements** and behavioral elements into **progressively larger subsystems**, and the architecture style that guides this organization -- these elements and their interfaces, their collaborations, and their composition.

## 48. Sofware Architecture Views

Known as 4+1 Views

1. Logical View (or Structural View) - an object model of the design

2. Process View (or Behavioral View) - concurrency and synchronization aspects

3. Development View (or Implementation View) – static organization (subset) of the software

4. Physical View (or Deployment View) - mapping of the software to the hardware

+1. Use-cases View (or Scenarios) - various usage scenarios

## 49. TOGAF: Architecture Types

The Open Group Architecture Framework (TOGAF) is an enterprise architecture methodology that offers a high-level framework for enterprise software development. TOGAF helps organize the development process through a systematic approach aimed at reducing errors, maintaining timelines, staying on budget and aligning IT with business units to produce quality results.

#### Pillars of TOGAF

There are 4 pillars of TOGAF which are the way to achieve the goals.  

##### Pillar 1: Business Architecture. 

Includes information on business strategy governance, organization and how to adapt any existing processes within the organisation. It’s becoming more and more popular these days.

##### Pillar 2: Application Architecture. 

For structuring and deploying application systems and in accordance with business goals, other organisational frameworks and all core business processes.  It ensures the application landscape is agile, scalable, reliable and manageable. 

##### Pillar 3: Data Architecture. 

Defines the organization’s data storage, management and maintenance, including logical and physical data models. Without well-planned data architecture, another type of architecture rises to take its place - “spaghetti architecture” - when every business unit or department sets out to buy its own solutions.

##### Pillar 4: Technical Architecture. 

It describes all necessary hardware, software and IT infrastructure involved in developing and deploying business applications. Provides a blueprint, for the infrastructure cost optimization program.

![1558618285958](assets/1558618285958.png)

**Business** **(Process) Architecture** -- addresses the needs of users, planners, and business management, 

**Data (Information) Architecture** -- addresses the needs of database designers, database administrators, and system engineers, 

**Application** **Architecture** -- addresses the needs of system and software engineers, and 

**Technology Architecture** -- addresses the needs of acquirers, operators, administrators, and managers.

## 50. The Role of Software Architecture

Software architecture is the fundamental organization of a system

It’s embodied in its components, their relationships to each other and the environment and the principles governing its design and evolution

Software architecture encompasses:

- The set of significant decisions about the organization of a software system
- Selection of the structural elements and their interfaces
- Behavior among those elements
- Composition of these elements into larger subsystems
- Architectural style that guides the organization

Software architecture enables the engineers to reason about the functionality and properties of a software system without getting involved in low-level source code and implementation details.

## 51. 4+1 View Model of Architecture

1. Logical View (or Structural View) - an object model of the design

2. Process View (or Behavioral View) - concurrency and synchronization aspects

3. Development View (or Implementation View) – static organization (subset) of the software

4. Physical View (or Deployment View) - mapping of the software to the hardware

+1. Use-cases View ( or Scenarios) - various usage scenarios

![1558623074442](assets/1558623074442.png)

## 52. Unified Process Phases

##### Inception

Establish the business case for the system, define risks, obtain 10% of the requirements, estimate next phase effort.

##### Elaboration

Develop an understanding of the problem domain and the system architecture, risk significant portions may be coded/tested, 80% major requirements identified.

##### Construction

System design, programming and testing.  Building the remaining system in short iterations.  

##### Transition

Deploy the system in its operating environment.  Deliver releases for feedback and deployment.

## 53. RUP: Inception Outcomes

- Clarify the Vision
- initial backlog,
- preliminary estimates, 
- one possible solution, 
- validation of the business case,
- business risk mitigated, 
- staleholder agreement

## 54. RUP:Elaboration Outcomes

- Architectural spikes completed, 
- architecturally significant backlog done, 
- validated system architecture, 
- walking skeleton, 
- product backlog fully defined, 
- technical risk mitigated, 
- backlog re-estimated,
- business decision to move forward

## 55. RUP:Construction Outcomes

- Product built, 
- product fully tested, 
- product documentation completed, 
- logistical risks mitigated, 
- business decision to release to market

## 56. RUP:Transition Outcomes

- Product finalized and ready to be released,
- any remaining issues resolved, 
- preparation for handoff, 
- transition to support or operations, 
- production deployment

## 57. The RUP—A Customizable Process Product

#### Best practices

The RUP comes with a library of best practices, produced by IBM Software and its partners. These best practices are continually evolving and cover a broader scope than this book can cover. The best practices are expressed in the form of phases, roles, activities, artifacts, and workflows.

#### Process delivery tools

The RUP is literally at the developers' fingertips because it is delivered online using Web technology, rather than using books or binders. This delivery allows the process to be integrated with the many software development tools in the Rational Suite and with any other tools so developers can access process guidance within the tool they are using.

#### Configuration tools

The RUP's modular and electronic form allows it to be tailored and configured to suit the specific needs of a development organization.

## 58. The Structure of Essential SAFe

#### 1. Lean-Agile principles

+ incremental builds
+ decentralized decision making

#### 2. Real Agile Teams and Trains

SAFe consists of real agile teams, that have everything and everyone to produce a working and tested increment of the solution.

#### 3. Cadence and Synchronization

IDK

#### 4. Program increment (PI) planning

- All stakeholders face-to-face, whenever possible
- Management sets the mission with minimum possible constraints
- Important stakeholder decisions are made immediately
- Requirements and design emerge
- Teams create and take responsibility for plans

#### 5. Improve DevOps and Releasability

DevOps improves collaboration and flow between Development and IT Operations with a continuous delivery pipeline.

#### 6. Get Fast Feedback with the System Demo

Demo full system increment to stakeholders every iteration in production like env (staging).

#### 7. Improve with Inspect & Adapt 

Review of PI outcomes and continuous improvement.

#### 8. Dedicate Time for Innovation and Planning (IP)

IDK

#### 9. Enable Fast Feature Delivery with Architectural Runway

Make current solutions so that they support the implementation of near-future important features.

#### 10. Lean-Agile leadership

- Management is trained in lean thinking.
- Lean leadership leads the way.

## 59. SAFe: Take an economic view

Delivering the best value and quality for people and society in the shortest sustainable lead time requires a fundamental understanding of the economics of building systems. It’s critical that everyday decisions are made in a proper economic context. The primary aspects include developing and communicating the strategy for incremental value delivery, establishing budgets and guardrails, and creating the economic framework for solution development.

## 60. SAFe: Implement Agile Teams and Agile Release Trains

Real Agile Teams and  Agile Release Trains (ARTs) are fully cross-functional. They have  everything, and everyone, necessary to produce a working, tested increment of the solution. They are self-organizing and self-managing,  which enables value to flow more quickly, with a minimum of overhead. Product Management, System Arch/Eng, and Release Train Engineer provide content and technical authority, and an effective development process.  Product Owners and Scrum Masters help the Dev teams meet their PI  Objectives. The Agile teams should engage the customer throughout the  development process.

## 61. Project Management & Portfolio Management

**Project Management** is about  **“doing projects right”** after the projects have been accepted for inclusion in the project portfolio.
Project **Portfolio Management** is about **“doing the right projects”**, namely, ensuring that only those projects are selected for subsequent inclusion in the project portfolio which will add value to the organization.

## 62. Software Product Quality

 Software quality measures how well **software is designed** (quality of design), and how well the **software conforms to that design** (quality of conformance):

Product quality - conformance to requirements or program specification
Scalability - Correctness  
Completeness  
Absence of bugs  
Fault-tolerance - Extensibility & Maintainability
Documentation  

## 63. Process and Product Quality

The quality of a developed product is influenced by the quality of the production process.

Particularly important in software development as some product quality attributes are hard to assess.

## 64. What is Software Testing?

- Testing is a process of executing the program with intent to **finding the bugs**
- Testing is the set of processes which ensure that the **functionality and performance planned** through design has been **delivered** through the system.
- Testing is **required to improve the quality** of the product before handling over the product to the customer.

## 65. Limitations of Testing

- To test all possible **inputs** is impractical or impossible
- To test all possible **paths** is impractical or impossible
- Testing can be used to show the presence of bugs, but never their absence. Dijkstra 1974.

## 66. Verification & Validation

**Verification** refers to the set of activities that ensure that software **correctly implements a specific function**.

**Validation** refers to a different set of activities that ensure that the software that has been built is **traceable to customer requirements**.

Boehm:
​	Verification: “Are we building the product right?”
​	Validation: “Are we building the right product?”

## 67. Unit Test

To verify that the component/module functions properly.

Test logic, internal data structures, boundaries.

Usually white box oriented.

Usually unit tests are written by devs. 

## 68. Integration Test

To ensure that code is implemented and designed properly  to take unit tested modules and build a program structure that has been dictated by design.

Combining the individual components to uncover errors associated with interfacing

Usually white box oriented

Tests are usually written by development team.

## 69. System Test

Purpose: To ensure that the system does what the requirement specifies 
Types

- Function Testing
- Performance Testing
- Installation Testing 

Usually Black box oriented
Tests are written by independent test team

## 70. Acceptance Test

**Customer’s** way to verify that what was wanted is what is built

- Uncovers more than requirements discrepancies
- Allows the customers to determine what they really want, whether specified in the document or not.
- New problems may arise
- Customers may decide that the problem as changed and a different solution is needed

Usually black box oriented

Tests are written by Customer/independent test team

## 71. Black Box Testing

No knowledge of internal design or code is required. Tests are based on requirements/specs.

## 72. White Box Testing

Knowledge of internal design and code is required. Tests are based on coverage of the code statements, branches, conditions.

## 73. Regression Testing

Re-testing after fixes or modifications of the software or its environment.

## 74. Performance Testing

Testing how well an application complies to performance requirements. 

## 75. Alpha Testing

Testing done when development is nearing completion. Minor design changes may still be made as a result of such testing.

## 76. Beta Testing

Testing when development and testing are essentially completed and final bugs and problems need to be found before release.

## 77. Smoke Testing

Smoke Testing is a quick-and-dirty **test that the major functions** of a piece of software work. 

Originated in the hardware testing practice of turning on a new piece of hardware for the first time and considering it a **success if it does not catch on fire**. 

In software testing, a smoke test is a collection of written tests that are performed on a system prior to being accepted for further testing. 

This is also known as a **build verification test**. 

## 78. A/B Testing

A/B testing (bucket tests or split-run testing) is a **randomized experiment with two variants**, A and B.

It includes application of statistical hypothesis testing or "two-sample hypothesis testing" as used in the field of statistics. 

A/B testing is a way to compare two versions of a single variable, typically by testing a subject's response to variant A against variant B, and determining which of the two variants is more effective.

As the name implies, two versions (A and B) are compared, which are identical except for one variation that might affect a user's behavior. 

Version A might be the currently used version (control), while version B is modified in some respect (treatment).

## 79. Top-down Testing

Start with high-level system and integrate from the top-down replacing individual components by stubs where appropriate

![1558698407967](assets/1558698407967.png)

## 80. Bottom-up Testing

Integrate individual components in levels until the complete system is created

![1558698424419](assets/1558698424419.png)

