# Tarkvaratehnika loengukonspekt

- Paul Leis

- [Moodle](https://moodle.taltech.ee/course/view.php?id=15556)

---

**Seminarid on kohustuslikud**, loengud soovituslikud

---

## Waterfall and iterative development models

Waterfall

- protsessid toimuvad üksteise järgi, pole pöörduv.
- Big Requirements Up Front (BRUF) - suured nõuded protsessi alguses
  - süsteeminõuded 
    - tarkvara nõuded 
      - analüüs
        - disain
          - kodeerimine
            - testimine
              - operatsioonid (deployment)

Iteratiivne arendus

- Teeme tükikaupa, näitame tükikaupa toodet jne, saame tagasisidet jooksvalt

Iga iteratsioon

- Selged eesmärgid
- Selgete use casede töö
- Võetakse kõige raskemad asjad esmalt (suurima riskiga asjad)

Iteratiivse arenduse riskid vähenevad palju kiiremini, kui kosemudeli puhul (kuna iter. tehakse riskantsed asjad esmalt)

Riskid:

- ehitad vale asja õigesti
- ehitad rohkem kui vaja

Muutmise hind

- Muutus on kosemudelil palju kallim (hooldusfaasis on vea parandamine 100x kallim, kui arendusfaasis), iteratiivsel on muudatuse hind pidevalt peaagu sama.
  kosemudeli puhul on muudatus alguses odavam kui iteratiivsel.

 ## Agiilne tarkvaraarendus

### User Stories

- US are short, simple description of a feature told from the perspective of the person who desires the new capability, usually a user or customer of the system

- Triple-C 

  - Card

    Eeldus suhtluseks

  - Conversation

    Suhtlus kliendi ja arendaja vahel

  - Confirmation

    testide kirjutamine, vestluse tulemuste kinnitamiseks ja täpsustamiseks.

![1548938648068](assets/1548938648068.png)

US võib olla ka alam USe

Liiga suuri US kutsutakse *Epicuteks*.

Detailid lisatud testidena:

![1548939687676](assets/1548939687676.png)

## 

Story, Theme and Epic. 

Epic on kõige suurem. Theme on kogum US. US on kasutaja poolt soovitud funktsionaalsus.

----

Iteratsiooni plaan on *freezed*, (mõne nädala plaan). 
Ajahinnangud antud. 

Tarkvaraarenduse hinnangud:

Traditsioonilised:

- Lines of Code (LoC)

  Function Points (Hinnangud failide arvu/keerukuse pealt)

Agiilne mõõtmine:

- Story points

  Ideaalsed päevad (1 story point on 8h ideaalne progemispäev).

Story Points. Story point values (1,2,3,5,8,13,20,40,100, ?).

Hinnangute andmine:

- Kõhutunne
- Analoogia
  - On millegi teise moodi.
  - Kahe erineva story põhjal hinnangu andmine.
- Planning poker
  - Kvantitatiivne ajurünnak.
  - Kõikide arvamus on kuulda
  - Kiire

Storyde prioriteseerimine peab käima kliendi mitte arendaja järgi. Arendaja peab andma siiski ajahinnangu.

Arendusmeeskonnale saab ülesandeid anda **vaid üks** inimene.

Hinnangud:

1. Teadmatuse määr. Kui palju tuleb enne uusi teadmisi omandada, kui arendame
2. Kui palju riski väheneb.
3. Kui palju toote väärtus tõuseb
4. Kui palju arendus maksab.

---

## Ekstreemprogrammeerimine (XP)

- Tarkvara arenduse metoloogia.
- Fokuseerib "Kõige lihtsamale asjale, mis töötab"
- Välja mõeldud **Kent Beck**i poolt.
- Eriti lühikesed iteratsioonid.

| Other (Waterfall?)                                          | XP                                                           |
| ----------------------------------------------------------- | ------------------------------------------------------------ |
| Nõudmiste koguminte alguses (hiljem ei muuda)               | Pidev. Planeeritakse iga iteratsiooniga. (Sama ka scrumis)   |
| Kui kliendi poolt heaks kiidetud, siis enam ei saa muutuda. | Võib muutuda iga iteratsiooniga                              |
| Ei kirjelda arenduspraktikaid.                              | Kirjeldab kindlaid arenduspraktikaid.<br />Tiimis saavad olla vaid eksperdid. |

Kui suudad jälgida seda, siis on head tulemused.

Neli muutujat? (*Four variables*)

- skoop (funktsionaalsus)
- kvaliteet (alati MAX, ei muutu)
- aeg
- raha.

Kõiki 3 (4) muutujat ei saa valida. Üks kolmest on funktsioon, mitte vabalt valitud muutuja. 

Tarkvaraprojektis on rohkem nõudeid, mis peavad olema (*must*), kui  *nice to have* (*Should/could*) nõudeid.

![1549541076643](assets/1549541076643.png)

Enamus aega ja raha peab panema must nõuetele (funktsionaalsus). 

---

Kolm tüüpi tarkvara funktsionaalsust:

- Baseline/Treshold - peavad olema olemas, et kasutaja oleks rahul
- Lineaarsed - mida rohkem seda parem (nice to have)
- Exiters/Delighters - kasutaja ei tea, mida ta tahab enne, kui ta seda näeb.

---

XP väärtused ja praktikad. **NB!** tuleb eksamil teada

XP programeerimiseks tuleb kasutada **kõiki** praktikaid ja väärtusi.

![1549542080172](assets/1549542080172.png)

Väärtused:

1. Kommunikatsioon. Verbaalne suhtlus on oluline kõikide tarkvaraprotsessis olevate osapoolte vahel. Suhtlus kliendi-arendaja-managementi vahel.
2. Lihtsus. Realiseerige kõige lihtsam varsioon kliendi vajaduste põhjal. Keerukus tekitab lisaprobleeme.
3. Tagasiside. Kõige võimsam kvaliteedi tagasiside instrument. Probleemid avastatakse kõige varaseimal võimalikul hetkel.
   Tagasiside liigid:
   1. Release plan (kuud)
   2. iteratsiooni plaan (nädalad)
   3. Acceptance tests (päevad)
   4. Stand-up (iga päev)
   5. Paaris arutamine (tunnid)
   6. Unit-tests (minutid)
   7. Paarisprogemine (kohene tagasiside)
4. Julgus. Julgus proovida uusi ideid ja lähenemisi. (**Ekstreemne** tarkvaralooja). Kommunikatsioon või tagasiside ütlevad, kui midagi ei tööta.

### 12 praktikat

- Planning game - planeerimine. Planeeritakse täpselt lähedal olevaid asju. Täpselt planeeritud iteratsioon ja realese. Aga mitte järgmised iteratsioonid.

- Small releses.  Release toimub tihedalt. Ajaliselt piiratud. Võimalikult väike, aga ärilise väärtusega. Kiire kliendi tagasiside, kas tegime õiget asja?

- Metafoor. Visioon. Nagu suur US. Millest saavad ühtemoodi aru kliendid ja arendajad.
  *Elevator statement*. Kirjeldab toodet kahe minutiga.

- Simple Design. Lihtne disain tagab lihtsa koodi.

- Refaktoreerimine. Tehniline praktika koodi lihtsamaks tegemiseks. Kirjutame koodi ümber jättes sama funktsionaalsuse. Või ka koodi omaduste täiendamine. 
  Bad smells in code:

  - Long methods / classes
  - duplicate code
  - methods does several different things
  - too much dependencies
  - complex / unreadable code

- TDD. Tagasiside viis. User story jaoks kirjutatakse vastuvõtutestid. Aitavad täpsustada funktsionaalsust. 
  Testid aitavad kliendil ja arendajal omavahel suhelda.
  Testid dokumentatsioonina
  Testid õpetamaks kuidas implementeerida
  Testid annavad tagasisidet, kui hästi asjad töötavad

- Paarisprogrammeerimine. Driver (hiir ja klaviatuur / mõtleb järgmise LoC peale ja API peale.) ja navigator(jälgib tööd/ mõtleb alternatiivide peale/ testide peale/ nõuete peale). 
  Lisaks: Paaris õppimine, Paaris arutlemine, Paaris *code review*. 
  Paarid peavad pidevalt vahetuma. 

- Collective Ownership. Kõik vastutavad kogu koodi eest. Tiim omab koodi. Kõik arendajad saavad muuta kogu koodi. 

- Continuous Integration. 
  Koodijupp integreeritakse **arenduskeskkonda** niipea, kui Unit Testid passivad. (Commit early and often).
  Edasiarendus CD (tootmiskeskonda *live*). 

- 40-Hour Workweek. Tööpäev peab olema väsitav, kuid ei tohi võtta rohkem aega. Arendajale peab jääma vaba aega.

- On-site Customer. Kliendid on kohapeal (US arutlemisel, teeb kriitilisi äriotsuseid), arendajad ei pea otsuseid ootama, face-to-face vestlus vähendab arusaamatusi.

- Coding Standards.
  Use coding coonventsions:

  - Rules for naming, formating, etc
  - Write readable and maintainable code

  Method commenting:

  - Self-documenting code
  - Don't comment bad code, rewrite it!

  Refactor to improve design
  Use code audit tools (CheckStyle, linters).

13. praktika? Stand-up

![1550143619646](assets/1550143619646.png)

### XP elütsükkel

![1550145717265](assets/1550145717265.png)

XP ei räägi *production* faasist.

![1550145830833](assets/1550145830833.png)

## Scrum

Eeldused:

- Klient leiab, mida ta tahab
- Arendaja teab kuidas seda ehitada
- Asjad võivad ajas mutuda

![1550148461022](assets/1550148461022.png)

- Scrum is an agile process that allows us to focus on delivering the highest business value in the shortest time. 
- It allows us to rapidly and repeatedly inspect actual working software (every two weeks to one month).
  The business sets the priorities. Teams self-organize to determine the best way to deliver the highest priority features. 
- Every two weeks to a month anyone can see real working software and decide to release it as is or continue to enhance it for another sprint.

> Uued asjad tekivad kaose ja korra piiril.

SCRUM is a lightweight process for managing and controlling software and product development in rapidly changing environments.

- Iterative, incremental process
- Team-based approach
- developing systems/ products with rapidly changing requirements
- Controls the chaos of conflicting interest and needs
- Improve communication and maximize cooperation
- Protecting the team form **disruptions** and impediments
- A way to maximize productivity

-----

### Scrum Omadused

- **Iseorganiseeruv** meeskond (Jagatud/kollektiivne vastutus)
- Toode areneb mitmete (1)-2-4 nädalat ajaliste "*sprintidega*"
- Nõudmised on salvestatud "*product backlogi*"
- Arendusmeetodeid pole kirjeldatud
- Üldised reeglid agiilse keskonna jaoks
- "agile" protsess.

### Scrumi protsess

![1550745050193](assets/1550745050193.png)

Kui sprint review failib, siis tehakse uus sprint, samade eesmärkidega.

#### Sprint planning

- Product backlogist -> sprint backlogi -> taskid/issued
- Sprint backlog on tiimi oma
- Product backlog on toote omaniku oma (Product owner)
- Sprint backlogil peab olema goal.
- Sprindi ajal ei tohi sprint muutuda. (Retros võib seda arutada.)



![1550749900814](assets/1550749900814.png)





#### Product increment

- Peab olema valmis "*Potentsially Shippable Product Increment*"

Mingi jama, mida tuleb arvestusel teada

![1550746243905](assets/1550746243905.png)

Ceremonies === Protsessid

----

Roles

- Product owner - vahendab vestlust tiimi ja huvitatud osapoolte vahel (*stakeholders*)
  - Defines features of the product
  - Owns the product backlog
  - Decides on release date and content
  - Responsible for profitability of the product (ROI)
  - Prioritize features according to market value
  - Adjust features and priority every iteration
  - Accept or reject work result
- Scrum Master
  - Represents management of the project
  - Responsible for enacting Scrum values and practices
  - Removes impediments 
  - Ensure that the team is fully functional and productive
  - Enable close cooperation across all roles and functions
  - **Shield** the team from **external** interferences
  - (Servent leader)
- 

Suure tiimi puhul peab palju omavahel suhtlema, arenduseks ei jää aega. Optimaalne tiim (5-9)

----

- Sead - PO, scrumi tiim, Scrum master

- Kanad - *stakeholders and users*

  ![1550747108839](assets/1550747108839.png)



---



## Kanban

Tööd ei lükata vaid tõmmatakse. (pulled not pushed).

Ehk tööd võetakse ise juurde.

Kanban board: (to do -> in development -> code review -> in testing -> done)

Pole vajadust "vaheladude jaoks".

Kanban on lean (*timmitud*), ehk arendusest on eemaldatud liiasus/muda, ehk need komponendid, mis ei loo lisaväärtust.



Kanban tehnika tuleb toyota autotöötlusest 1950'ndatest.

Kanban tarkvaraarendus on aastast 2007 (David Anderson).

![1551803100405](assets/1551803100405.png)

Tarkvaraarenduses on palju mõttetut funktsionaalsust. Peame loobuma liigsest, et oleks timmitud (*lean*).

Kõik, mis võtab ressursse, aga ei loo väärtust on liiasus/muda.

Kanban põhiline eesmärk on **liiasuse (*waste*) vähendamine**.

Seitse liiasust (*waste*):

![1551803293190](assets/1551803293190.png)

Scrumi miinused:

+ backlogide haldamine võtab liiga kaua aega
+ time-boxing võib tööd häirida (iteratsioonid)
+ estimation võib liiga palju aega võtta, pole kasu.



----

Kanbani omaduse:

- Tööd tõmmatakse mitte ei lükata

- Pole iteratsioone

- Keskendutakse pidevale töövoole.

- work-in-progress on tahvlil limiteeritud. On mingi max arv, mis on lubatud igas kategoorias (to-do, development, test).

  ![1551803769205](assets/1551803769205.png)

Tõmbamise (*pull*) reeglid:

- Don’t build features that nobody needs right now
- Don’t write more specs than you can code
- Don’t write more code than you can test
- Don’t test more code than you can deploy



**MMF - Minimal marketable feature** is a chunk of functionality that delivers a subset of the customer’s requirements, and that is capable of returning value to the customer when released as an independent entity.

![1551804340955](assets/1551804340955.png)

MMF võrreldes US ja themega.

![1551804367844](assets/1551804367844.png)



Kanban planeerimine:

- The ideal work planning process should always provide the development team with best thing to work on next, no more and no less.
- Further planning beyond this does not add value and is therefore waste.

### Kanban vs scrum

Kanban piirab WIP arvu staatuse järgi (testimisel, arendamisel), scrum piirab WIP arvu iteratsioonis.

Scrum ei luba iteratsiooni ajal muutust. Kanbanis pole iteratsiooni.

![1551804662537](assets/1551804662537.png)

Kaardi E lisamisel: Võib lisada `To Do` tahvlile, aga siis peab C või D eemaldama. Kui A või B saab tehtud võetakse `To Do` tahvlilt kõige ülemine.



Scrum tahvel on iga sprindi lõpuks tühi. Kanbani tahvel näeb koguaeg enamvähem sama välja.

Scrumis peavad tiimi liikmed saama kõigega hakkama (test, arendus). Kanbanis võivad olla selleks spetsiaalsed inimised.



Scrumis peab iga backlogi item mahtuma iteratsiooni. Kanbanis on lubatud pikemad taskid.

![1551804892373](assets/1551804892373.png)

![1551804916108](assets/1551804916108.png)



Scrumis peab andma aja/mahu arvestusi. Kuna muidu ei saa planeerida.

![1551805026958](assets/1551805026958.png)



### Kanbani koosolek

![1551805162960](assets/1551805162960.png)

### J-kurv

Evolutsioon vs revolutsioon.

![1551805290140](assets/1551805290140.png)



### 5 põhjust kanbaniks

1. **Ability to release anytime** – Scrum and XP, usually do not release in the middle of the sprint. This is not the case with Kanban.
2. **Ability to change priorities on the fly** – Scrum is reluctant to change the priorities in the middle of the sprint. In Kanban, if there is an urgent request to implement or a really important user story, the team can just put it on top of the queue.
3. **No need in iterations** – Iterations are perfect for getting into a rhythm. However, after a point, when the flow is established, iterations could rather become a waste.
4. **No need in estimates** – Just as iterations, estimates could also become a waste. You can take the most important user story from backlog and implement it.
5. **Perfect flow visualization** - Kanban Board provides a very clear view on current work in progress. It visualizes flow and enables fast planning and tracking.

## Lean startup (timmitud idufirma)

What is startup:

![1551807612023](assets/1551807612023.png)

- Eric Ries on raamatu Lean Startup autor.
- Startup == Eksperiment

What is lean startup?

![1551807689446](assets/1551807689446.png)

![1551807700299](assets/1551807700299.png)

![1551807713556](assets/1551807713556.png)

Lean Startup algab **visioonist**. / inimeste vajadustest/valupunktist.

#### Kliendi leidmise protsess

(Pivot ehk suunamuutus.)

![1551807945725](assets/1551807945725.png)



- customer discovery - kes on meie klient? Kelle suunas on visioon suunatud

- customer validation - kas on ikka meie klient?

  ![1551808366538](assets/1551808366538.png)

#### Pivot

Change of strategy based on validate learning – a special kind of change designed to test a new fundamental hypothesis about the product, business model, and engine of growth.
A pivot requires that we keep one foot rooted in what we’ve learned so far, while making a fundamental change in strategy.

> If you don't know what you're doing ... better do it fast.

Leans startupi eesmärk on leida lahendus probleemile, mis pole veel teada.

![1551808431768](assets/1551808431768.png)

![1551808485883](assets/1551808485883.png)

#### Why before what

>Most startups don't fail because they can't build a great product, but because no one wanted the product.



Planeerimise faasi enne ehitamist ei ole.

![1551808662526](assets/1551808662526.png)

### Validated learning

“Learning” is often used as an excuse for a failure of execution or after-the-fact rationalization. To draw a clear distinction, Lean Startup uses the word “Validated”.
“Validated Learning” is the process of demonstrating empirically that a team has discovered valuable truths about a startup’s present and future business prospects.

![1551808721121](assets/1551808721121.png)



### Minimum Viable Product

- Version of product that enables a full turn of the Build – Measure – Learn loop with a minimum amount of effort and the least amount of development time.
- Has core features which allow the product to be deployed.
- Helps in avoiding building a product that the customers do not want.
- Can be deployed to Early Adopters that are thought to be more likely to provide feedback.

![1551808971728](assets/1551808971728.png)

![1551808989712](assets/1551808989712.png)

What lean start-up is not

![1551809074257](assets/1551809074257.png)



## Lean UX

> User eXperience design

![1553164571686](assets/1553164571686.png)



![1553164716898](assets/1553164716898.png)

![1553165367217](assets/1553165367217.png)

> Agile: Working software is the primary measure of progress. 
>
> LEAN Startup: Validated learning is the primary measure of progress.
>
> LEAN UX: Delivered value is the primary measure of progress.



## DevOps

![1553774269669](assets/1553774269669.png)

- Viga tarkvaras tekitab süsteemi mittetöötamist
- Productionis on raske viga tuvastada
- “Manual error” is a commonly cited root cause

### Probleem:

Current IT operating models are not designed for today’s high-velocity business environment

![1553774655306](assets/1553774655306.png)

IT-operatsioonide tiimi väärtused on arendusmeeskonnaga võrreldes erinevad.

![1553774760554](assets/1553774760554.png)

### Deming's 9.th principle

>Break down barriers between departments. People in research, design, 
>sales, and production must work as a team,  to foresee problems of 
>production and usage that may be encountered with the product or 
>service.
>
>-W. Edwards Deming

![1553775072538](assets/1553775072538.png)



### Lahendus

Dev ja operatsiooni tiim peavad omavahel suhtlema, jagama teadmisi. 

Enhance Service design with operational knowledge

- Reliablility
- Performance
- Security
- Test them

It calls for automDevOps is a philosophy under which the business teams, development teams, and the operations organization collaborate on a continuous basis to make sure that IT solutions are available to business on time and that they run without disruption. ation, collaboration, cultural change, and an organizational structure that is less complex and is easy to navigate. 

It addresses the people, process, and tools, as well as the technology dimensions needed to secure this collaboration and sync up the different stakeholders to move functionality to production faster.

**Devops as business process**

✓ Release planning
✓ Continuous integration
✓ Continuous delivery
✓ Continuous testing
✓ Continuous deployment
✓ Continuous monitoring and feedback

---

Some organizations, such as Netflix, don’t have separate development and operations teams;
instead, a single “NoOps” team owns both sets of responsibilities.

Other organizations have succeeded with DevOps liaison teams, which resolve any conflicts and promote collaboration.

----

**Involvement of Ops team**

A fundamental requirement of DevOps is that the Ops team is continuously engaged with the development team throughout the life cycle of solution development. 
Ops should participate right from the visioning stage to understand the business vision, the epics, and the release time lines. 

They should also contribute to determining the solution's technical and schedule feasibility.
From the visioning stage through the development stage, the Ops team should provide the necessary inputs to the development team in order for them to build and validate the Ops-related requirements. 

### DevOps Principles

The Three Ways

- Systems Thinking
- Amplify Feedback Loops
- Culture of Continual Experimentation

CAMS 

- Culture – People > Process > Tools
- Automation – Infrastructure as Code
- Measurement – Measure Everything
- Sharing – Collaboration/Feedback

**Informed by the values in the Agile Manifesto and Lean Theory of Constraints**

---

### Continuous Integration, Delivery and Deployment

![1553776345194](../../../../S%C3%BCsteemide%20arendus/4.%20Semester/public/docs/assets/1553776345194.png)

Continuous Integration - arenduskeskkonda

Continuous Delivery - testkeskonda

Continuous Deployment - productionisse

#### Continuous Integration

- Continuous Integration emerged in the Extreme Programming (XP) community, and XP advocates Martin Fowler and Kent Beck first wrote about continuous integration circa 1999. 
- **Continuous Integration** is a software development practice where members of a team integrate their work **frequently**, usually each person integrates at least daily - leading to multiple integrations per day.
- Each integration is **verified** by an **automated build** (including test) to detect integration errors as quickly as possible. 

**CI best practices**

- Single Source Repository.
- Automate the Build and Test
- Everyone Commits Every Day
- Keep the Build Fast
- Everyone can see what's happening
- Automate Deployment (Optional)

**Why CI?**

Integration is hard, effort increase exponentially with

- Number of components
- Number of bugs
- Time since last integration

#### Continuous Deployment

Why?

![1553776554439](assets/1553776554439.png)

### DevSecOps

![1553776857961](assets/1553776857961.png)

Progressive orgs have prioritized security due to uptime and compliance concerns

DevSecOps is the answer to integrating these various challenges into a coherent and effective approach to software delivery. 

It is a new method that helps identify security issues early in the development process rather than after a product is released.

![1553777348175](assets/1553777348175.png)

DevSecOps: Integrate security into DevOps

![1553777430008](assets/1553777430008.png)

## Architecture

behavioral <-> dünaamiline

structural <-> staatiline

![1554372735743](assets/1554372735743.png)

